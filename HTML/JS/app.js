var app = angular.module("snakeApp", ["ngRoute"]);

app.config(function($routeProvider,$locationProvider) {
  $routeProvider
  .when("/menu", {
    templateUrl : "HTML/TEMP/menu.html",
    controller: "menuController"
  })
  .when("/scores", {
    templateUrl : "HTML/TEMP/scores.html",
    controller: "scoresController"
  })
  .when("/options", {
    templateUrl : "HTML/TEMP/options.html",
    controller: "optionsController"
  })
  .when("/about", {
    templateUrl : "HTML/TEMP/about.html",
    controller: "aboutController"
  })
  .when("/game", {
    templateUrl : "HTML/TEMP/game.html",
    controller: "gameController"
  })
  .otherwise({
    redirectTo: "/menu"
  });
  $locationProvider.html5Mode(true);
});

app.controller("snakeController", function($scope,$interval) {
  $scope.appData = {
    difficulty:0,
    currentScore:0,
    userName:"",
    userSubmited:false,
    loading: false,
    timer: 3,
    bgPos: 0,
    userSubmit: function () {
      this.userSubmited = true;
      this.loading = true;
      let x = this;
      var a = $interval(function () {
        if(x.timer == 0){
          $interval.cancel(a);
          x.loading = false;
        }
        x.timer -= 1;
      },1000);
    }
  }
});

app.controller("menuController", function($scope) {
  $scope.appData.bgPos = 0;
});

app.controller("scoresController", function($scope) {
  $scope.appData.bgPos = 25;
  $scope.scoreData = {
    scores: [
      {username: "Tijana", points: 255, date: "22/08/2019"},
      {username: "Filip", points: 250, date: "21/08/2019"},
      {username: "Bogdan", points: 240, date: "19/08/2019"},
      {username: "Uros", points: 200, date: "15/08/2019"},
      {username: "Pera", points: 190, date: "12/08/2019"},
      {username: "Zika", points: 170, date: "11/08/2019"},
      {username: "Mika", points: 150, date: "10/08/2019"},
      {username: "Aljosa", points: 145, date: "09/08/2019"},
      {username: "Jasmina", points: 140, date: "22/08/2019"},
      {username: "Ivan", points: 135, date: "21/08/2019"}
    ]

  }

});

app.controller("optionsController", function($scope) {
  $scope.appData.bgPos = -25;
});

app.controller("aboutController", function($scope) {

});

app.controller("gameController", function($scope, $interval) {
  $scope.gameData = {
    userName: "",
    points: 0,
    gameOver: false,
    snakeInterval: null,
    updateDirection: function (x) {
      if ($scope.appData.userSubmited == true && $scope.appData.loading == false) {
        if ((x.key == "ArrowUp" || x.key == "w") && this.snake.snakeDirection().disallowedDirection != "TOP") {
          this.snake.currentDirection = "TOP";
        }
        else if ((x.key == "ArrowDown" || x.key == "s") && this.snake.snakeDirection().disallowedDirection != "BOTTOM") {
          this.snake.currentDirection = "BOTTOM"
        }
        else if ((x.key == "ArrowLeft" || x.key == "a") && this.snake.snakeDirection().disallowedDirection != "LEFT") {
          this.snake.currentDirection = "LEFT";
        }
        else if ((x.key == "ArrowRight" || x.key == "d") && this.snake.snakeDirection().disallowedDirection != "RIGHT") {
          this.snake.currentDirection = "RIGHT";
        }
        else {
          this.snake.currentDirection = this.snake.snakeDirection().defaultDirection;
        }
        console.log(this.snake.currentDirection);
      }
    },
    snake: {
      // width: 1-74
      // height: 1-48
      snakePositions: [{x: 65,px: 65, py: 38, y: 38,snakeObj: null},
                       {x: 65,px: 65, py: 39, y: 39,snakeObj: null},
                       {x: 65,px: 65, py: 40, y: 40,snakeObj: null}],
      currentDirection: "TOP",
      food: {
        x: 40,
        y: 40,
        foodObj: null
      },
      snakeDirection: function () {
        // zabranjena direkcija
        var ret = {
          disallowedDirection: null,
          defaultDirection: null,

        }
        if (this.snakePositions[0].x > this.snakePositions[1].x) {
          ret.disallowedDirection = "LEFT";
          ret.defaultDirection = "RIGHT";
        }
        else if (this.snakePositions[0].x < this.snakePositions[1].x) {
          ret.disallowedDirection = "RIGHT";
          ret.defaultDirection = "LEFT";
        }
        else if(this.snakePositions[0].y > this.snakePositions[1].y) {
          ret.disallowedDirection = "TOP";
          ret.defaultDirection = "BOTTOM";
        }
        else if(this.snakePositions[0].y < this.snakePositions[1].y) {
          ret.disallowedDirection = "BOTTOM";
          ret.defaultDirection = "TOP";
        }
        return ret;



      },
      clearFragment: function (x) {
        console.log("clearFragment", x);
         x.snakeObj.clearRect(10*x.x, 10*x.y, 10, 10);
      },
      clearFoodFragment: function (x) {
        console.log("clearFragment", x);
         x.foodObj.clearRect(10*x.x, 10*x.y, 10, 10);
      },
      foodColision: function () {
        if (this.snakePositions[0].x == this.food.x && this.snakePositions[0].y == this.food.y) {
          return true;
        } else {
          return false;
        }
      },
      headColision: function () {
        if (this.snakePositions[0].x == 0 ||
            this.snakePositions[0].x == 75 ||
            this.snakePositions[0].y == 0 ||
            this.snakePositions[0].y == 49) {
          return true;
        } else {
          return false;
        }
      },
      snakeColision: function (x,y) {
        for (var i = 0; i < this.snakePositions.length; i++) {
          if (this.snakePositions[i].x == x && this.snakePositions[i].y == y) {
            return true;
          }
        }
        return false;
      },
      redraw: function () {
        let snakeCanvas = document.getElementById("snake-game");

        this.food.foodObj = snakeCanvas.getContext("2d");
        this.clearFoodFragment(this.food);
        this.food.foodObj.beginPath();
        this.food.foodObj.rect(10*this.food.x, 10*this.food.y, 10, 10);
        this.food.foodObj.fillStyle = "#0000ff";
        this.food.foodObj.fill();
        this.food.foodObj.closePath();


        for (let i = 0; i < this.snakePositions.length; i++) {
          this.snakePositions[i].snakeObj = snakeCanvas.getContext("2d");
          this.clearFragment(this.snakePositions[i]);
        }

        //Moving
        let lastPos = {
          x:0,
          y:0
        }
        for(let i = $scope.gameData.snake.snakePositions.length-1; i >= 0; i--){
          if(i == 0){
            //glava
            var x = 0;
            var y = 0;

            if ($scope.gameData.snake.currentDirection == "TOP") {
              y -= 1;
            }
            else if ($scope.gameData.snake.currentDirection == "BOTTOM") {
              y += 1;
            }
            else if ($scope.gameData.snake.currentDirection == "LEFT") {
              x -= 1;
            }
            else if ($scope.gameData.snake.currentDirection == "RIGHT") {
              x += 1;
            }

            $scope.gameData.snake.snakePositions[i].x += x;
            $scope.gameData.snake.snakePositions[i].y += y;

          } else {
            if (i == $scope.gameData.snake.snakePositions.length-1) {
              lastPos.x = $scope.gameData.snake.snakePositions[i].x;
              lastPos.y = $scope.gameData.snake.snakePositions[i].y;
            }
            $scope.gameData.snake.snakePositions[i].x = $scope.gameData.snake.snakePositions[i-1].x;
            $scope.gameData.snake.snakePositions[i].y = $scope.gameData.snake.snakePositions[i-1].y;
          }
        }

        if(this.foodColision() == true) {
          //uvecava zmiju
          var newFrag = {x: lastPos.x, y: lastPos.y,snakeObj: null};
          this.snakePositions.push(newFrag);
          this.snakePositions[this.snakePositions.length - 1].snakeObj = snakeCanvas.getContext("2d");
          this.clearFragment(this.snakePositions[this.snakePositions.length - 1].snakeObj);

          // pojava hrane na drugom mestu
          this.food.foodObj = snakeCanvas.getContext("2d");
          this.clearFoodFragment(this.food);

          var newFoodCords = {
            x: 0,
            y: 0,
          }
          while(newFoodCords.x!=0 ||
                newFoodCords.x!=75 ||
                newFoodCords.y!=0 ||
                newFoodCords.y!=49 ||
                this.snakeColision(newFoodCords.x, newFoodCords.y)
          ){
            newFoodCords.x = Math.floor(Math.random()* 76);
            newFoodCords.y = Math.floor(Math.random()* 50);
          }

          this.food.x = newFoodCords.x;
          this.food.y = newFoodCords.y;

          this.food.foodObj.beginPath();
          this.food.foodObj.rect(10*this.food.x, 10*this.food.y, 10, 10);
          this.food.foodObj.fillStyle = "#0000ff";
          this.food.foodObj.fill();
          this.food.foodObj.closePath();
        }

        //Igra gotova ( Game over )
        if (this.headColision() == true) {
          console.log("Game over");
          $interval.cancel($scope.gameData.snakeInterval);
        }
        else {
          for (let i = 0; i < this.snakePositions.length; i++) {

            this.snakePositions[i].snakeObj.beginPath();
            this.snakePositions[i].snakeObj.rect(10*this.snakePositions[i].x, 10*this.snakePositions[i].y, 10, 10);
            this.snakePositions[i].snakeObj.fillStyle = "#ff0000";
            this.snakePositions[i].snakeObj.fill();
            this.snakePositions[i].snakeObj.closePath();

          }
        }

      }
    },
    updateUsername: function () {
      $scope.appData.userSubmit();
      this.userName = $scope.appData.userName;
      setTimeout(function () {

        //Making an interval
        $scope.gameData.snakeInterval = $interval(function () {
          $scope.gameData.snake.redraw();
        },100);

      },4000);
    }
  }

  window.addEventListener("keyup", function (e) {
    $scope.gameData.updateDirection(e);
  });

  var snakeCanvas = document.getElementById("snake-game");


  if ($scope.appData.difficulty == 0) {

    var canvasBorder = snakeCanvas.getContext("2d");
    canvasBorder.beginPath();
    canvasBorder.rect(0, 0, 760, 10);
    canvasBorder.rect(0, 490, 760, 10);
    canvasBorder.rect(0, 10, 10, 480);
    canvasBorder.rect(750, 10, 10, 480);
    canvasBorder.fillStyle = "#000000";
    canvasBorder.fill();
    canvasBorder.closePath();

  } else if ($scope.appData.difficulty == 1) {
    var canvasBorder = snakeCanvas.getContext("2d");
    canvasBorder.beginPath();
    canvasBorder.rect(0, 0, 760, 10);
    canvasBorder.rect(0, 490, 760, 10);
    canvasBorder.rect(0, 10, 10, 480);
    canvasBorder.rect(750, 10, 10, 480);
    canvasBorder.rect(200, 150, 360, 10);
    canvasBorder.rect(200, 340, 360, 10);
    canvasBorder.fillStyle = "#000000";
    canvasBorder.fill();
    canvasBorder.closePath();
  }








});
